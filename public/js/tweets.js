var map;
var markers = [];
var geocoder; 
var infoWindow;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -33.8688, lng: 151.2195},
        zoom: 13
    });

    geocoder = new google.maps.Geocoder();

    document.getElementById('submit').addEventListener('click', function () {
        geocodeAddress(geocoder, map);
    });


}

function geocodeAddress(geocoder, resultsMap) {
    var address = document.getElementById('search_input').value;


    geocoder.geocode({'address': address}, function (results, status) {
        if (status === 'OK') {

            /* Get Only City */
            var $city = false;

            jQuery.each(results[0].types, function (m, type) {
                if (type == 'locality' || type == 'administrative_area_level_1') {
                    $city = true;
                    return false;
                }
            })

            if ($city) {


                var place = results[0];

                if (place.geometry.viewport) {
                    resultsMap.fitBounds(place.geometry.viewport);
                    resultsMap.setOptions({draggable: true});
                } else {
                    resultsMap.setCenter(place.geometry.location);
                    resultsMap.setZoom(14);
                }

                search_tweets(place.geometry.location.lat(), place.geometry.location.lng(), place.address_components[0].long_name)
                resultsMap.setCenter(results[0].geometry.location);



            } else {
                alert('Not found any city by given name: ' + address);
            }


        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
}

// Shows any markers currently in the array.
function showMarkers() {
    setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
    clearMarkers();
    markers = [];
}

window.onresize = function () {
    var currCenter = map.getCenter();
    google.maps.event.trigger(map, 'resize');
    map.setCenter(currCenter);
};
function search_tweets(lat, long, city) {
    jQuery.get(search_url + "/" + lat + "/" + long + "/" + city, function (json) {
        deleteMarkers();
        jQuery.each(json, function (key, data) {
            var latLng = new google.maps.LatLng(data.coordinates[1], data.coordinates[0]);

            var marker = new google.maps.Marker({
                position: latLng,
                map: map,
                icon: {
                    url: data.profile_image_url,
                },
            });

            //Attach click event handler to the marker.
            google.maps.event.addListener(marker, "click", function (e) {
                if (infoWindow) {
                    infoWindow.close();
                }

                infoWindow = new google.maps.InfoWindow({
                    content: '<p>Tweet: ' + data.text + '</p>' + '<p>When : ' + data.created_at + '</p>'
                });
                infoWindow.open(map, marker);
            });

            markers.push(marker);

        });
    }, 'json');
}

jQuery('#search_input').keypress(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) { //Enter keycode                        
        jQuery('#submit').click();
    }
})