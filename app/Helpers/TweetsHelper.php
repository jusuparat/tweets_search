<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Cache;
use Thujohn\Twitter\Facades\Twitter;
use Carbon\Carbon;

class TweetsHelper {

    /**
     * To look for tweets by given conditions. First look into the cache first 
     * if not found anything from cache by the key call Twitter API to get new tweets and store it 
     * to the cache for specific amount of time which can be change the time from .env file
     * 
     * @param float $lat Latitude of searching location 
     * @param float $long Longitude of searching location
     * @param string $city The name of searching location
     * 
     * @return array Array of Tweets
     */
    public static function getTweets($lat, $long, $city) {

        $cachekey = $lat . $long;
        $distance = env('TWITTER_WITHIN_DISTANCE', '50km');
        $geocode = $lat . ',' . $long . ',' . $distance;

        $tweets = array();

        if (!Cache::has($cachekey)) {

            $_tweets = Twitter::getSearch(['geocode' => $geocode, 'q' => $city, 'format' => 'array', 'count' => 1000]);

            if (!isset($_tweets['statuses'])) {
                return $tweets;
            }

            if (empty($_tweets)) {
                return $tweets;
            }

            foreach ($_tweets['statuses'] as $tw) {

                if (!isset($tw['coordinates']['coordinates'])) {
                    continue;
                }

                if (stripos($tw['text'], $city) === false) {
                    continue;
                }

                $t = [
                    'text' => $tw['text'],
                    'created_at' => Carbon::parse($tw['created_at'])->format('F j, Y H:i'),
                    'coordinates' => $tw['coordinates']['coordinates'],
                    'profile_image_url' => isset($_SERVER["HTTPS"]) ? $tw['user']['profile_image_url_https'] : $tw['user']['profile_image_url'],
                ];
                array_push($tweets, $t);
            }

            /**
             * Store results in to cache for specific amount of time
             * 
             * Get setting from env file 
             */
            $cache_minutes = env('TWITTER_RS_TIMEOUT', 60);
            $expiresAt = Carbon::now()->addMinutes($cache_minutes);

            Cache::put($cachekey, $tweets, $expiresAt);
        } else {

            $tweets = Cache::get($cachekey);
        }

        return $tweets;
    }

}
