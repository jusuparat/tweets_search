<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\TweetsHelper;
use Illuminate\Support\Facades\Cookie;
use App\Search;

class TweetsController extends Controller {

    /**
     * Return JSON response of tweets
     * 
     * @param float $lat Latitude of searching location 
     * @param float $long Longitude of searching location
     * @param string $city The name of searching location
     * 
     * @return strin JSON Response
     */
    public function search($lat, $long, $city) {

        $this->store_search($city);
        $a = TweetsHelper::getTweets($lat, $long, $city);

        return response()->json($a);
    }

    /**
     * To store current user search term by cookie session key
     * 
     * @param string $city The name of searching location
     */
    function store_search($city) {

        $cookie = Cookie::get('laravel_session');

        $_city_rs = Search::where('key', $cookie)
                ->where('value', $city)
                ->first();

        if (!empty($_city_rs)) {
            return;
        }

        $_search = new Search;
        $_search->key = $cookie;
        $_search->value = $city;
        $_search->save();
    }

    /**
     * To return user search history view page
     * Display the list of search history
     */
    function history() {

        $cookie = Cookie::get('laravel_session');
        $cities = Search::where('key', $cookie)->get();
        return view('history', ['cities' => $cities]);
    }

}
