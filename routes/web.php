<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('maps');
});


//Route::get('/search', function() {
//    $a = TweetsHelper::getTweets(34.0522342, -118.2436849, 'Los Angeles');
//    print count($a);
//    print "<pre>" . print_r($a, true) . "</pre>";
//});

Route::get('/search-tweets/{lat}/{long}/{city}', 'TweetsController@search');
Route::get('/search-history', 'TweetsController@history');
