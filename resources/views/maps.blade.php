<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Tweets Search</title>

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">


        <!-- Custom styles for this template -->
        <link href="{{ asset('css/styles.min.css') }}" rel="stylesheet">


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="map-container">
            <div id="map"></div>
        </div>
        <div class="search">
            <input id="search_input" class="controls" type="text" placeholder="Enter a location" >
            <input id="submit" class="button text-uppercase" type="submit" value="Search">
            <!--<input  class="button text-uppercase" type="button" value="History">-->
            <a href = '{{url('/search-history')}}' class="button text-uppercase text-center">History</a>
        </div>

        <script>
            var search_url = "{{url('/search-tweets')}}";
        </script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAG-KilTr6kL8GhXzzDKFCMPTfU2JFpw3o&libraries=places&callback=initMap" async defer></script>
        <script type="text/javascript" src="{{ asset('js/tweets.min.js') }}"></script>
    </body>
</html>
